from django import HttpResponse, HttpResponseNotFound
from django.shortcuts import render, redirect

from .models import *
menu = ["О сайте", "Добавить пост", "Обратная связь", "Войти"]


def index(request):
    posts = Women.objects.all()
    return render(request, 'myapp/index.html', {'posts': posts, 'menu': menu, 'title': ' Главная страница'})


def about(request):
    return render(request, 'myapp/about.html', {'menu': menu, 'title': 'О сайте'})


def categories(request, cat):
    return HttpResponse(f"<h1>Статьи по категориям</h1>{cat}</p>")


def archive(request, year):
    if (int(year) > 2022):
        return redirect('home', permanent=True)

    return HttpResponse(f"<h1>Архив по годам</h1>{year}</p>")


def pageNotFound(request, exception):
    return HttpResponseNotFound('<h1>Страница не найдена</h1>')


